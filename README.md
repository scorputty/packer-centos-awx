# Vagrant Ansible AWX Packer CentOS-7 with gitlab runner
A set of [(Ansible)](https://www.ansible.com) scripts around [Packer by HashiCorp](https://www.packer.io/) to build a [CentOS 7](https://www.centos.org) box for [Oracle VM VirtualBox](https://www.virtualbox.org).

# This build uses scorputty/centos as a base-image.
https://app.vagrantup.com/scorputty/boxes/centos

- building AWX Tower on top of CentOS Vagrant box with Gitlab runner binary installed.

```sh
./build.sh
```
