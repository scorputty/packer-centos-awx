#!/usr/bin/env bash

# This script pulls the base image and sets the environment variables for packer

export BOX_REPO="scorputty"
export BOX_NAME="centos"

# Grab the box
vagrant box add ${BOX_REPO}/${BOX_NAME} --provider virtualbox

# Grab version
VERSION=$(vagrant box list |grep ${BOX_REPO}/${BOX_NAME} | cut -d"(" -f2 | cut -d")" -f1 | cut -d" " -f2)

export BOX_VERSION=${VERSION}

echo "Using repository:${BOX_REPO} and box:${BOX_NAME} with version:${BOX_VERSION} as base"

packer build -force centos7-awx-from-basebox.json
